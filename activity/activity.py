# 1. Create an abstract class called Animal that has the following abstract methods:
#a. eat(food)
#b. make_sound()

class Dog():
	def eat(food):
		print("Eaten Steak")

	def make_sound(self):
		print("Bark! Woof! Arf!")

class Cat():
	def eat(food):
		print("Serve Tuna")

	def make_sound(self):
		print("Miaow! Nyaw! Nyaaaaa!")

dog1 = Dog()
cat1 = Cat()

dog1.eat()
dog1.make_sound()

cat1.eat()
cat1.make_sound()


# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
	# a. Properties:
	# 	i. Name
	# 	ii. Breed
	# 	iii. Age

	# b. Methods
	# 	i. Getters
	# 	ii. Setters
	# 	iii. Implementation of abstract methods
	# 	iv. call()

class Dog():
	def __init__(self, name, breed, age):
		super().__init__() # Gives access to the properties of the parent class (which is the Person class)
		self._name = name
		self._breed = breed
		self._age = age

# getter method

	def get_name(self):
		print(f"Here {self.name}")

# setter method

	def set_name(self, name):
		self._name = name

# call()

	def get_call(self):
		print(f"Here {self._name}!")

dog1 = Dog("Isis","Dalmatian",12)
dog1.get_call()


class Cat():
	def __init__(self, name, breed, age):
		super().__init__() # Gives access to the properties of the parent class (which is the Person class)
		self._name = name
		self._breed = breed
		self._age = age

# getter method

	def get_name(self):
		print(f"{self.name}, come on!")

# setter method

	def set_name(self, name):
		self._name = name

# call()

	def get_call(self):
		print(f"{self._name}, come on!!")

cat1 = Cat("Puss","Persian",4)
cat1.get_call()




